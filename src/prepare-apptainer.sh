#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SRC="$(cd "$(dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd)"

# shellcheck source=./src/containerlib-apptainer.sh
source "$SRC"/containerlib-apptainer.sh
source "$SRC"/message.sh

msg_info "PREPARE" "Apptainer: pulling image $CUSTOM_ENV_CI_JOB_IMAGE to $CE_IMAGEFILE" >&2

set +o nounset
if [ -z "$CUSTOM_ENV_IMAGEFILE" ]
then
    CUSTOM_ENV_IMAGEFILE=''
fi
if [ -z "$CUSTOM_APPTAINER_USE_OVERLAYFS" ]
then
    CUSTOM_APPTAINER_USE_OVERLAYFS=10240
fi
set -o nounset

CUSTOM_ENV_CI_JOB_IMAGE=docker://${CUSTOM_ENV_CI_JOB_IMAGE}

if [[ -z "$CUSTOM_ENV_IMAGEFILE" ]] || [[ ! -a "$CUSTOM_ENV_IMAGEFILE" ]]
then
    COMMAND=(apptainer
             pull
             "$CE_IMAGEFILE"
             "$CUSTOM_ENV_CI_JOB_IMAGE")

    msg_info "PREPARE" "Command: ${COMMAND[*]}" >&2
    "${COMMAND[@]}" || {
        msg_error "" "ERROR: command failed: ${COMMAND[*]}";
        msg_error "" "       Image pull failed";
        }
    msg_info "" "Verify existence of file:" >&2
    ls "$CE_IMAGEFILE" >&2
fi

if [[ -z "$CUSTOM_APPTAINER_USE_OVERLAYFS" ]] || [[ ! -a "$CUSTOM_APPTAINER_USE_OVERLAYFS" ]]
then
    COMMAND=(apptainer
             overlay create -S
             -s ${CUSTOM_APPTAINER_USE_OVERLAYFS}
             -f "$CE_IMAGEFILE"
             )

    msg_info "PREPARE" "Command: ${COMMAND[*]}" >&2
    "${COMMAND[@]}" || {
        msg_error "" "ERROR: command failed: ${COMMAND[*]}";
        msg_error "" "       Image pull failed";
        }
    msg_info "" "Verify existence of file:" >&2
    ls "$CE_IMAGEFILE" >&2
fi
