#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

SRC="$(cd "$(dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd)"
# shellcheck source=./src/include.sh
source "$SRC/include.sh"

set +o nounset
source "$SRC/message.sh"
msg_info "PREPARE" "Using workspace $CE_QUEUE_WORKSPACE..." >&2

CONTAINERISATION_BACKEND="${1}"

[[ -n "$CUSTOM_ENV_CI_JOB_IMAGE" ]] || export CUSTOM_ENV_CI_JOB_IMAGE=""
[[ -n "$CUSTOM_ENV_IMAGEFILE" ]] || export CUSTOM_ENV_IMAGEFILE=""
[[ -n "$CUSTOM_ENV_APPTAINER_DOCKER_USERNAME" ]] || export CUSTOM_ENV_APPTAINER_DOCKER_USERNAME=""
[[ -n "$CUSTOM_ENV_APPTAINER_DOCKER_PASSWORD" ]] || export CUSTOM_ENV_APPTAINER_DOCKER_PASSWORD=""
[[ -n "$CUSTOM_ENV_LOG_LEVEL" ]] || export CUSTOM_ENV_LOG_LEVEL=2
set -o nounset

# Both defined
if [[ -n "$CUSTOM_ENV_CI_JOB_IMAGE" ]] && [[ -n "$CUSTOM_ENV_IMAGEFILE" ]]
then
    msg_detail -n "" "image: $CUSTOM_ENV_CI_JOB_IMAGE"
    msg_detail -n "" " and variable IMAGEFILE=$CUSTOM_ENV_IMAGEFILE"
    msg_detail "" " are set at the same time."
    if [[ -a "$CUSTOM_ENV_IMAGEFILE" ]]
    then
        msg_error "" "$CUSTOM_ENV_IMAGEFILE does exist, not dowloading image: $CUSTOM_ENV_CI_JOB_IMAGE"
    fi
fi

# image: undefined but IMAGEFILE defined
if [[ -z "$CUSTOM_ENV_CI_JOB_IMAGE" ]] && [[ -n "$CUSTOM_ENV_IMAGEFILE" ]]
then
    if [[ ! -a "$CUSTOM_ENV_IMAGEFILE" ]]
    then
        msg_error "" "IMAGEFILE=$CUSTOM_ENV_IMAGEFILE does not exist and image: not specified."
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi
fi
msg_info -n "PREPARE" " "; validate_containerisation_backend \
                             "$CONTAINERISATION_BACKEND" \
                             "$CUSTOM_ENV_CI_JOB_IMAGE" \
                             "$CUSTOM_ENV_IMAGEFILE"



if [[ -n "$CUSTOM_ENV_CI_JOB_IMAGE" ]] || [[ -n "$CUSTOM_ENV_IMAGEFILE" ]]
then
    msg_info "PREPARE" "Using image $CUSTOM_ENV_CI_JOB_IMAGE" >&2

    case "$CONTAINERISATION_BACKEND" in
        enroot)
            PREPARE_SCRIPT="$SRC/prepare-enroot.sh"
            ;;
        apptainer)
            PREPARE_SCRIPT="$SRC/prepare-apptainer.sh"
            ;;
        *)
            msg_error "PREPARE" "Error: No valid container backend specified"
            msg_error "PREPARE" "       (i.e., apptainer or enroot)"
            msg_error "PREPARE" "       but job requires a container image."
            exit 22
            ;;
    esac
    WRAP="./wrap-prepare-$CUSTOM_ENV_CI_JOB_ID.sh"

    # We cannot submit the scripts as they are
    # because the custom executor queue logic
    # will change their location,
    # then BASH_SOURCE[0] will not point to the original location
    # when they are executed
    # and they will not be able to find
    # the other scripts in the repository.
    cat <<EOF > "$WRAP"
#!/bin/bash
export APPTAINER_DOCKER_USERNAME="$CUSTOM_ENV_APPTAINER_DOCKER_USERNAME" # TODO: add test
export APPTAINER_DOCKER_PASSWORD="$CUSTOM_ENV_APPTAINER_DOCKER_PASSWORD" # TODO: add test
export CUSTOM_ENV_CI_JOB_IMAGE="$CUSTOM_ENV_CI_JOB_IMAGE"
export CUSTOM_ENV_IMAGEFILE="$CUSTOM_ENV_IMAGEFILE"
export CUSTOM_ENV_CI_JOB_ID="$CUSTOM_ENV_CI_JOB_ID"
export CUSTOM_ENV_CI_CONCURRENT_ID="$CUSTOM_ENV_CI_CONCURRENT_ID"
export CUSTOM_ENV_LOG_LEVEL="$CUSTOM_ENV_LOG_LEVEL"
source "$PREPARE_SCRIPT"
EOF
    chmod +x "$WRAP"

    # shellcheck source=./src/custom_executor_queue.sh
    source "$SRC/custom_executor_queue.sh"
    # shellcheck disable=SC1091
    source "$CE_QUEUE_WORKSPACE/.env"

    msg_info "" "Wrap to be executed (credentials redacted):" >&2
    msg_info "" "==========================" >&2
    msg_info "" $(grep -vi "PASSWORD\|USERNAME" "$WRAP") >&2
    msg_info "" "==========================" >&2
    submit_to_custom_executor_queue_and_wait "$WRAP" "JOB_$CUSTOM_ENV_CI_JOB_ID"
else
    msg_info "PREPARE" "No image selected." >&2
fi
