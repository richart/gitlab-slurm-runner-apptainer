#!/usr/bin/env bash

if [[ x"$CUSTOM_ENV_LOG_LEVEL" == "x" ]]; then
    export CUSTOM_ENV_LOG_LEVEL=1
fi


function print_msg() {
    ctxt=$1
    shift

    args=""
    if [[ "$ctxt" == "-n" ]]; then
        ctxt=$1
        args="-n"
    fi

    if [[ -n "$ctxt" ]]; then
        ctxt="[$ctxt]"
    fi

    echo $args $ctxt "$@"
}

function msg_error() {
    if [[ "$CUSTOM_ENV_LOG_LEVEL" -ge 1 ]]; then
        print_msg "$@"
    fi
}

function msg_info() {
    if [[ "$CUSTOM_ENV_LOG_LEVEL" -ge 2 ]]; then
        print_msg "$@"
    fi
}

function msg_detail() {
    if [[ "$CUSTOM_ENV_LOG_LEVEL" -ge 3 ]]; then
        print_msg "$@"
    fi
}
