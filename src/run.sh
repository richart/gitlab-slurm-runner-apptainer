#!/usr/bin/env bash

if [[ "$#" -ne 3 ]]
then
    echo "Usage: run.sh <containerisation_backend> <script> <stepname>" >&2
    exit "$SYSTEM_FAILURE_EXIT_CODE"
fi

CONTAINERISATION_BACKEND="${1}"
ORIGINAL_SCRIPT="${2}"
STEPNAME="${3}"

SRC="$(cd "$(dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd)"
source "$SRC"/message.sh

msg_info "RUN" "Running script:$2 step:$3 on backend:$1"

if [[ -z "$CE_QUEUE_WORKSPACE" ]]
then
    msg_error "RUN" "CE_QUEUE_WORKSPACE not defined." >&2
    exit "$SYSTEM_FAILURE_EXIT_CODE"
fi


# shellcheck source=./src/run-include.sh
source "$SRC"/run-include.sh

msg_info -n "RUN"; validate_containerisation_backend \
                     "$CONTAINERISATION_BACKEND" \
                     "$CUSTOM_ENV_CI_JOB_IMAGE" \
                     "$CUSTOM_ENV_IMAGEFILE"


if [[ -n "$CUSTOM_ENV_CI_JOB_IMAGE" ]] || [[ -n "$CUSTOM_ENV_IMAGEFILE" ]]
then
    case "$CONTAINERISATION_BACKEND" in
        enroot)
            WRAPPER_NAME=wrap_enroot
            ;;
        apptainer)
            WRAPPER_NAME=wrap_apptainer
            ;;
        *)
            msg_error "RUN" "Error: No valid container backend specified"
            msg_error "RUN" "       (i.e., apptainer or enroot)"
            msg_error "RUN" "       but job requires a container image."
            exit 22
            ;;
    esac
    case "$STEPNAME" in
        prepare_script|\
            step_*|\
            step_script|\
            build_script|\
            after_script|\
            cleanup_file_variables)

        F="$WRAPPER_NAME"

        _BNAME="$(basename "$ORIGINAL_SCRIPT")"
        SCRIPT=$(mktemp "$CE_QUEUE_WORKSPACE/cp-XXXXXX${_BNAME}")
        cat "$ORIGINAL_SCRIPT" > "$SCRIPT"
        chmod +x "$SCRIPT"

        ;;

        get_sources|\
            restore_cache|\
            download_artifacts|\
            archive_cache|\
            archive_cache_on_failure|\
            upload_artifacts_on_success|\
            upload_artifacts_on_failure)

        F="nowrap"
        SCRIPT="$ORIGINAL_SCRIPT"

        ;;

        *)
            msg_error "RUN" "Step name not recognized: $STEPNAME" >&2
            exit "$SYSTEM_FAILURE_EXIT_CODE"
        ;;
    esac
else
    F="nowrap"
    SCRIPT="$ORIGINAL_SCRIPT"
fi

# shellcheck source=./src/custom_executor_queue.sh
source "$SRC/custom_executor_queue.sh"
# shellcheck disable=SC1091
source "$CE_QUEUE_WORKSPACE/.env"

WRAPPED="$("$F" "$SCRIPT")"

msg_info "RUN" "Submitting wrapped script ($WRAPPED) to custom executor queue..."
msg_info "RUN" "Wrap to be executed (credentials redacted):" >&2
if [[ "$WRAPPED" == "$ORIGINAL_SCRIPT" ]]
then
    msg_info "" "<same as received>"
else
    grep -vi "PASSWORD\|USERNAME" "$WRAPPED" >&2
fi

msg_detail "RUN" "Output:" >&2
submit_to_custom_executor_queue_and_wait "$WRAPPED" "JOB_$CUSTOM_ENV_CI_JOB_ID"
EXIT_CODE=$?
msg_detail "RUN" "Exit code: $EXIT_CODE" >&2
msg_detail "RUN" "End of Output:" >&2
exit "$EXIT_CODE"
