#!/usr/bin/env bash

# This is based on the tutorial for bats
# https://bats-core.readthedocs.io/en/stable/tutorial.html

function _common_setup() {
    load '../test_helper/bats-support/load' # for run
    load '../test_helper/bats-assert/load' # for assert_output and refute_output
    load '../test_helper/bats-file/load' # for filesystem-related asserts
    load '../test-utils'
    PROJECT_ROOT="$(cd "$(dirname "$BATS_TEST_FILENAME" )/../.." > /dev/null 2>&1 && pwd)"

    # To make scripts in /src available for sourcing
    # and loading
    SRC="$PROJECT_ROOT/src"
    PATH="$SRC:$PATH"

}


function _common_teardown() {
    rm -rf "$TEST_PLAYGROUND"
}

function _no_custom_executor_queue_variables_declared() {
    #local ndeclared=$(env | grep -E '^CUSTOM_EXECUTOR_QUEUE' | wc -l )
    local ndeclared
    ndeclared=$( echo "${!CUSTOM_EXECUTOR_QUEUE*}" | wc -w )
    [ "$ndeclared" -eq 0 ]
}


